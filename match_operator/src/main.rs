fn main() {
    value_in_cents(Coin::Quarter(UsState::Alaska));

    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);

    let x = five;
    //let x = none;
    match x {
        None => (),
        Some(val) => {
            println!("Value is: {}.", val);
        }
    }

    //let some_u8_value = 0;
    let some_u8_value = 1;
    match some_u8_value {
        1 => println!("one"),
        2 => println!("two"),
        3 => println!("three"),
        _ => (),
    }

    let x = &Coin::Dime;
    foo(&Coin::Quarter(UsState::Alaska));
    foo(x);
    foo(&Coin::Nickel);
}

#[derive(Debug)]
#[allow(unused)]
enum UsState {
    Alabama,
    Alaska,
    // --snip--
}

#[allow(unused)]
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Luck penny.");
            1
        }
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}.", state);
            25
        }
    }
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

fn foo(coin: &Coin) {
    let mut count = 0;
    match coin {
        Coin::Quarter(state) => {
            println!("State quarter from {:?}.", state)
        }
        _ => {
            count += 1;
        }
    }

    println!("Count is: {}.", count);

    // Better solution:

    let mut count = 0;
    if let Coin::Quarter(st) = coin {
        println!("State quarter from {:?}.", st);
    } else {
        count += 1;
    }

    println!("Count is: {}.", count);
}
