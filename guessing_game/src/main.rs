use std::io;
use std::cmp::Ordering;
use rand::Rng;

const MAX_NUMBER: i32 = 100;

/// Valid guessed value.
struct Guess {
    value: i32,
}

impl Guess {
    /// Create new guess.
    ///
    /// Panics on value outside of range 0..MAX_NUMBER.
    pub fn new(value: i32) -> Guess {
        if !(0..MAX_NUMBER).contains(&value) {
            panic!("Guess must be between 0 and {MAX_NUMBER}. Got {value} instead.");
        }
        Guess { value }
    }

    pub fn value(&self) -> i32 { self.value }
}

fn main() {
    println!("Guess the number.");

    let secret_number = rand::thread_rng().gen_range(0..MAX_NUMBER); // Alternatively, `1..101`
    // println!("The secret number is {secret_number}"); // DEBUG

    loop {
        let mut guess = String::new();
        println!("Please input your guess.");

        // Alternatively, without the 'use' statement: std::io::stdin()
        io::stdin()
            .read_line(&mut guess)
            .expect("Valid value for guess should be written on stdin.");

        // println!("You guessed: {guess}"); // DEBUG

        let x = 5;
        let y = 10;
        println!("x = {x} and y + 2 = {}", y + 2);


        let guess: Guess = match guess.trim().parse() {
            Ok(num) => Guess::new(num),
            Err(_) => continue,
        };

        match guess.value().cmp(&secret_number) {
            Ordering::Less => println!("Too small."),
            Ordering::Greater => println!("Too large."),
            Ordering::Equal => {
                println!("You win!");
                break;
            },
        }
    } // loop
}
