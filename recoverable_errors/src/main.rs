use std::fs::File;
use std::io::ErrorKind;
use std::io;
use std::io::Read;
use std::error::Error;

//fn main() -> Result<(), Box<dyn Error>> {
//    let f = File::open("hello.txt")?;
//
//    Ok(())
//}

fn main() {
    let file_name = "hello.txt";
    let f = File::open(file_name);
    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create(file_name) {
                Ok(created_file) => created_file,
                Err(error_creating_file) => panic!("Error with creating the file: {:?}", error_creating_file),
            },
            other_error => panic!("Problem with opening the given file: {:?}.", other_error),
        },
    };

    // The same functionality using closures.
    let file_name = "hello2.txt";
    let f = File::open(file_name).unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create(file_name).unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });

    let file_name = "hello3.txt";
    //let f = File::open(file_name).unwrap();
    let f = File::open(file_name).expect(&*format!("File '{}' could not be opened", file_name));
}

fn read_username_from_file(file_name: &str) -> Result<String, io::Error> {
    let f = File::open(file_name);
    let mut f = match f {
        Ok(file) => file,
        Err(error) => return Err(error),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(error) => Err(error),
    }
}

fn read_username_from_file2(file_name: &str) -> Result<String, io::Error> {
    let mut f = File::open(file_name)?;

    let mut s = String::new();

    f.read_to_string(&mut s)?;
    Ok(s)
}

fn read_username_from_file3(file_name: &str) -> Result<String, io::Error> {
    let mut s = String::new();
    File::open(file_name)?.read_to_string(&mut s)?;
    Ok(s)
}

fn read_username_from_file4(file_name: &str) -> Result<String, io::Error> {
    let mut s = String::new();
    File::open(file_name)?.read_to_string(&mut s)?;
    Ok(s)
}

fn read_username_from_file5(file_name: &str) -> Result<String, io::Error> {
    std::fs::read_to_string(file_name)
}

pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}", value);
        }

        Guess { value }
    }

    pub fn value(&self) -> i32 { self.value }
}
