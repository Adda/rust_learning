enum Suit {
    Clubs,
    Spades,
    Diamonds,
    Hearts,
}

enum IpAddrKind {
    IPV4,
    IPV6,
}

enum IpAddr_e {
    IPV4(String),
    IPV6(String),
    IPV4Sep(u8, u8, u8, u8),
}

struct IpAddr {
    version: IpAddrKind,
    address: String,
}

fn main() {
    let log = "BEGIN Transaction XK342
UPDATE 234:LS/32231 {\"price\": 31.00} -> {\"price\": 40.00}
DELETE 342:LO/22111";

    for line in log.lines() {
        let parse_result = parse_log(line);
        println!("{:?}", parse_result);
    }

    let ipv4 = IpAddrKind::IPV4;
    let ipv6 = IpAddrKind::IPV6;

    let home = IpAddr {
        version: IpAddrKind::IPV4,
        address: String::from("127.0.0.1"),
    };

    let loopback = IpAddr {
        version: IpAddrKind::IPV6,
        address: String::from("::1"),
    };

    let home_e = IpAddr_e::IPV4(String::from("127.0.0.1"));
    let loopback_e = IpAddr_e::IPV6(String::from("::1"));
    let home_e_sep = IpAddr_e::IPV4Sep(127, 0, 0, 1);

    let m = ActionMessage::Write(String::from("Hello"));
    let n = ActionMessage::Write(String::from("Test."));
    m.call();
    n.call();

    route(ipv4);
    route(ipv6);
}

fn route(ip_kind: IpAddrKind) {}

enum ActionMessage {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

enum Card {
    King(Suit),
    Queen(Suit),
    Jack(Suit),
    Ace(Suit),
    Pip(Suit, usize),
}

impl ActionMessage {
    fn call(&self) {
        println!("Called.");
    }
}

#[derive(Debug)]
enum Event {
    Update,
    Delete,
    Unknown,
}

type Message = String;

fn parse_log(line: &str) -> (Event, Message) {
    let parts: Vec<_> = line.splitn(2, ' ').collect();
    if parts.len() == 1 {
        return (Event::Unknown, String::from(line));
    }

    let event = parts[0];
    let rest = String::from(parts[1]);

    match event {
        "UPDATE" | "update" => (Event::Update, rest),
        "DELETE" | "delete" => (Event::Delete, rest),
        _ => (Event::Unknown, String::from(line)),
    }
}
