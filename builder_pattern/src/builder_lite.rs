use crate::shape::{Geometry, Material, MaterialType, Shape};

impl Shape {
    pub fn new() -> Shape {
        Shape {
            geometry: Geometry {
                length: 1,
                width: 1,
            },
            position: (0, 0, 0),
            material: None,
        }
    }

    pub fn with_geometry(mut self, length: u32, width: u32) -> Shape {
        self.geometry = Geometry { length, width };
        self
    }

    pub fn geometry(self, length: u32, width: u32) -> Shape {
        self.with_geometry(length, width)
    }

    pub fn with_material(
        mut self,
        material_type: MaterialType,
        durability: u32,
        weight: u32,
    ) -> Shape {
        self.material = Some(Material {
            material_type,
            durability,
            weight,
        });
        self
    }
}
