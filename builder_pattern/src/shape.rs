#[derive(Debug)]
pub struct Shape {
    pub position: Position,
    pub geometry: Geometry,
    pub material: Option<Material>,
}

#[derive(Debug)]
pub struct Material {
    pub material_type: MaterialType,
    pub durability: u32,
    pub weight: u32,
}

#[derive(Debug)]
pub enum MaterialType {
    Wood,
    Iron,
    Gold,
}

pub type Coordinate = i32;
pub type Position = (Coordinate, Coordinate, Coordinate);

#[derive(Debug)]
pub struct Geometry {
    pub length: u32,
    pub width: u32,
}
