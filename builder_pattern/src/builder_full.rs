use crate::shape::{Geometry, Material, Position, Shape};

pub struct ShapeBuilder {
    position: Option<Position>,
    geometry: Option<Geometry>,
    material: Option<Material>,
}

impl ShapeBuilder {
    pub fn new() -> ShapeBuilder {
        ShapeBuilder {
            position: None,
            geometry: None,
            material: None,
        }
    }

    pub fn position(&mut self, position: Position) -> &mut Self {
        self.position = Some(position);
        self
    }

    pub fn geometry(&mut self, geometry: Geometry) -> &mut Self {
        self.geometry = Some(geometry);
        self
    }

    pub fn material(&mut self, material: Material) -> &mut Self {
        self.material = Some(material);
        self
    }

    pub fn build(&mut self) -> Shape {
        Shape {
            position: self.position.unwrap_or((0, 0, 0)),
            geometry: self.geometry.take().unwrap_or(Geometry {
                length: 0,
                width: 0,
            }),
            material: self.material.take(),
        }
    }
}
