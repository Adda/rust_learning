use crate::builder_full::ShapeBuilder;
use crate::shape::{Geometry, Material, MaterialType, Shape};

mod builder_full;
mod builder_lite;
mod shape;

fn main() {
    let shape = Shape::new()
        .with_material(MaterialType::Wood, 34, 42)
        .with_geometry(3, 5);

    println!("{:?}", shape);

    let shape = ShapeBuilder::new()
        .position((0, -3, 5))
        .geometry(Geometry {
            length: 3,
            width: 4,
        })
        .material(Material {
            material_type: MaterialType::Iron,
            durability: 32,
            weight: 12,
        })
        .build();

    println!("{:?}", shape);
}
