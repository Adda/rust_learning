fn main() {
    let v1 = vec![1, 2, 3];
    let v1_iter = v1.iter();
    // Lazy iterators are not using any CPU or RAM atm.

    for val in v1_iter {
        println!("Got {}.", val);
    }
}
