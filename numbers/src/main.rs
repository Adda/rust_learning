use num::complex::Complex;
use std::{
    convert::TryFrom,
    rc::Rc,
    sync::{Arc, Mutex},
};

fn main() {
    let a = Complex { re: 2.1, im: -1.2 };
    let b = Complex::new(11.1, 22.2);
    let result = a + b;
    println!("{} + {}i", result.re, result.im);

    let a = 10;
    let b = Box::new(20);
    let c = Rc::new(Box::new(30));
    let d = Arc::new(Mutex::new(40));
    println!("a: {:?}, b: {:?}, c: {:?}, d: {:?}", a, b, c, d);

    // Binds a reference (&) to a mutable (mut) array ([...]) that contains 1,024 unsigned 8-bit
    //  integers (u8) initialized to 0 to the variable buffer.
    let _buffer = &mut [0u8; 1024];

    let a = 10;
    let b: i32 = 20;
    let c = 30i32;
    let d = 30_i32;
    let e: i32 = add(add(a, b), add(c, d));
    println!("( a + b ) + ( c + d ) = {}", e);
    let f = add_wrong(add(a, b), add(c, d)) as u32;
    println!("( a + b ) + ( c + d ) = {}", f);

    println!("{} == {}", 24.5_f32.round(), f32::round(24.5_f32));
    assert_eq!(24.5_f32.round(), f32::round(24.5_f32));

    let twenty = 20;
    let twenty_one: i32 = 21;
    let twenty_two = 22i32;

    let mut addition = twenty + twenty_one;
    addition += twenty_two;
    println!("{} + {} + {} = {}", twenty, twenty_one, twenty_two, addition);

    let one_million: i64 = 1_000_000;
    println!("{}", one_million.pow(2));

    let forty_twos = [
        42.0,
        42f32,
        42.0_f32,
    ];

    println!("{:02}", forty_twos[0]);

    let _three = 0b_11_i32;
    let _three = 0b11_i32;
    let three = 0b11;
    let thirty = 0o36;
    let three_hundred = 0x12C;
    println!("base 10: {} {} {}", three, thirty, three_hundred);
    println!("base 2: {:b} {:b} {:b}", three, thirty, three_hundred);
    println!("base 8: {:o} {:o} {:o}", three, thirty, three_hundred);
    println!("base 16: {:x} {:x} {:x}", three, thirty, three_hundred);

    let a: i32 = 10;
    let b: u16 = 100;
    if a < (b as i32) {
        println!("Ten is less than one hundred.");
    }

    let a: i32 = 10;
    let b: u16 = 100;
    let b = i32::try_from(b).unwrap();
    if a < b {
        println!("Ten is less than one hundred.");
    }

    let abc: (f32, f32, f32) = (0.1, 0.2, 0.3);
    let xyz: (f64, f64, f64) = (0.1, 0.2, 0.3);

    println!("abc (f32)");
    println!(" 0.1 + 0.2: {:x}", (abc.0 + abc.1).to_bits());
    println!(" 0.3: {:x}", (abc.2).to_bits());
    println!();
    println!("xyz (f64)");
    println!(" 0.1 + 0.2: {:x}", (xyz.0 + xyz.1).to_bits());
    println!(" 0.3: {:x}", (xyz.2).to_bits());
    println!();
    assert_eq!(abc.0 + abc.1, abc.2); // Just do not do this normally. Do not compare floats to equality.
    //assert_eq!(xyz.0 + xyz.1, xyz.2); // Would panic.

    let result = 0.1_f32 + 0.1;
    let desired = 0.2;
    assert!((desired - result).abs() <= f32::EPSILON);

    let x = (-42.0_f32).sqrt();
    assert_ne!(x, x);

    let x: f32 = 1.0 / 0.0;
    assert!(x.is_infinite());
    assert!(x.is_sign_positive());
    assert!(!x.is_sign_negative());
    assert!(!x.is_nan());
    assert!(!x.is_finite());

    let a = 42;
    let r = &a;
    let b = a + *r;
    println!("a + a = {}", b);

    let needle = 0o204;
    let haystack = [1, 1, 2, 5, 15, 36, 52, 132, 203, 877, 4140, 21147];
    for item in &haystack {
        if *item == needle {
            println!("{}", item);
        }
    }
}

fn add(i: i32, j: i32) -> i32 {
    i + j
}

fn add_wrong(i: i32, j: i32) -> i32 {
    i - j
}
