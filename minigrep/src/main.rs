use std::env;
use std::process;

use minigrep::Config;

fn main() {
    // Get command line arguments passed to the program.
    let args: Vec<String> = env::args().collect();
    dbg!(&args);
    // println!("{:?}", args);

    // Parse arguments into a Config struct.
    let config = Config::build(&args)
        //let config = Config::new(env::args()).
        .unwrap_or_else(|err| {
            eprintln!("Problem parsing arguments: {err}.");
            process::exit(1);
        });

    // Run the program with passed configuration.
    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {e}");
        process::exit(2);
    }
}
