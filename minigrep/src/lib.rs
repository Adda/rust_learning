use std::env;
use std::error::Error;
use std::fs;

#[derive(PartialEq, Default)]
pub enum CaseSensitive {
    #[default]
    False,
    True,
}

impl Into<bool> for CaseSensitive {
    fn into(self) -> bool {
        match self {
            CaseSensitive::False => false,
            CaseSensitive::True => true,
        }
    }
}

pub struct Config {
    query: String,
    filename: String,
    case_sensitive: CaseSensitive,
}

impl Config {
    //pub fn new(args: env::Args) -> Result<Config, &'static str> {
    pub fn build(args: &[String]) -> Result<Config, &'static str> {
        let mut args_iter = args.into_iter();
        args_iter.next(); // Skipping the program name.

        //let query = args[1].clone();
        //let filename = args[2].clone();
        //println!("Searching for '{}' in file '{}'.", query, filename);
        let query = match args_iter.next() {
            Some(arg) => arg,
            None => return Err("Did not get a query string."),
        };
        let filename = match args_iter.next() {
            Some(arg) => arg,
            None => return Err("Did not get a file name"),
        };

        let case_sensitive;
        if args.contains(&String::from("--case-insensitive")) {
            case_sensitive = CaseSensitive::False;
        } else if args.contains(&String::from("--case-sensitive")) {
            case_sensitive = CaseSensitive::True;
        } else if env::var("CASE_INSENSITIVE").is_err() {
            case_sensitive = CaseSensitive::True;
        } else {
            case_sensitive = CaseSensitive::False;
        }

        Ok(Config {
            query: query.to_string(),
            filename: filename.to_string(),
            case_sensitive,
        })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let file_contents = fs::read_to_string(&config.filename)?;
    //println!("The file '{}' content:\n{}", config.filename, file_contents);

    let results = search(&config.query, &file_contents, Some(config.case_sensitive));

    for found_line in results {
        println!("{found_line}");
    }

    Ok(())
}

pub fn search<'a>(
    query: &str,
    contents: &'a str,
    case_sensitive: Option<CaseSensitive>,
) -> Vec<&'a str> {
    let case_sensitive: bool = case_sensitive.unwrap_or(CaseSensitive::default()).into();
    let query = if case_sensitive {
        query.to_owned()
    } else {
        query.to_lowercase()
    };

    //let mut result = Vec::new();

    //for line in contents.lines() {
    //    if line.contains(query) {
    //        result.push(line);
    //    }
    //}

    //result

    contents
        .lines()
        .filter(|line| {
            let line = if case_sensitive {
                line.to_string()
            } else {
                line.to_lowercase()
            };
            line.contains(&query)
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_new_config() {
        let program_name = String::from("create_new_config()");
        let query = String::from("needle");
        let filename = String::from("haystack");
        let args = vec![program_name.clone(), query.clone(), filename.clone()];
        let config = Config::build(&args).unwrap();
        assert_eq!(config.query, query);
        assert_eq!(config.filename, filename);
    }

    #[test]
    fn create_config_without_enough_arguments() {
        let program_name = String::from("create_new_config()");
        let query = String::from("needle");
        let args = vec![program_name.clone(), query.clone()];
        let config = Config::build(&args);
        assert!(config.is_err());
    }

    #[test]
    fn create_config_without_any_arguments() {
        let program_name = String::from("create_new_config()");
        let args = vec![program_name];
        let config = Config::build(&args);
        assert!(config.is_err());
    }

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents, Some(CaseSensitive::True))
        );
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search(query, contents, Some(CaseSensitive::False))
        );
    }
}
