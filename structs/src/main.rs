#![allow(unused_variables)]

static mut ERROR: i32 = 0;

fn main() {
    let subject = AlwaysEqual; // Unit-like structs.

    let mut buffer: Vec<u8> = vec![];

    let mut f = File::new("something.txt");
    read(&f, &mut buffer);
    unsafe {
        if ERROR != 0 {
            panic!("An error has occurred while reading the file ")
        }
    }
    close(&mut f);
    unsafe {
        if ERROR != 0 {
            panic!("An error has occurred while closing the file ")
        }
    }

    let f3_data: Vec<u8> = vec![114, 117, 115, 116, 33];
    let mut f3 = File::new_with_data("2.txt", &f3_data);
    let mut buffer: Vec<u8> = vec![];
    open(&mut f3);
    let f3_length = f3.read(&mut buffer);
    close(&mut f3);
    let text = String::from_utf8_lossy(&buffer);
    println!("{:?}", f3);
    println!("{} is {} bytes long", &f3.name, f3_length);
    println!("{}", text);

    File {
        name: String::from("f1.txt"),
        data: Vec::new(),
    };
    File {
        name: String::from("f2.txt"),
        data: vec![114, 117, 115, 116, 33],
    };

    File::new_with_data("f1.txt", &vec![]);
    File::new_with_data("f2.txt", &vec![114, 117, 115, 116, 33]);

    let f3 = File::new("f3.txt");
    // Fields are private by default, but can be accessed within the module that defines the struct.
    let f3_name = &f3.name;
    let f3_length = f3.len();
    let f3_length = f3.data.len();

    println!("{:?}", f3);
    println!("{} is {} bytes long", f3_name, f3_length);

    let mut f2 = File {
        name: String::from("2.txt"),
        data: vec![114, 117, 115, 116, 33] };


    open(&mut f2);
    let f2_length = read(&f2, &mut buffer);
    close(&mut f2);

    let text = String::from_utf8_lossy(&buffer);

    println!("{:?}", f2);
    println!("{} is {} bytes long", &f2.name, f2_length);
    println!("{}", text);

    let mut user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    print_user(&user1);
    user1.email = String::from("test@example2.com");

    let user2 = build_user("useremail@mail.com", "username2", false, 0);
    print_user(&user2);

    let user3 = User {
        email: String::from("email@tutanota.com"),
        ..user1
    };
    print_user(&user3);

    let _black = Color(0, 0, 0);
    let _origin = Point(0, 0, 0);

    let mut f1 = File {
        name: String::from("f1.txt"),
        data: Vec::new(),
    };

    let f1_name = &f1.name;
    let f1_length = &f1.data.len();

    f1.data.push(23);
    f1.data.push(42);
    f1.data.push(67);

    println!("{:?}", f1);
    println!("{:#?}", f1);
    println!("{} is {} bytes long from ref and {} in fact.", f1_name, f1_length, f1.data.len());

    let ordinary_string = String::from("localhost");
    let host = Hostname ( ordinary_string.clone() );
    //connect(ordinary_string); // Will fail to compile: Expected Hostname, got String.
}

struct User {
    username: String,
    email: String,
    sign_in_count: i64,
    active: bool,
}

impl User {
    fn test(&self) -> String {
        let mut a = String::new();
        a.push_str(&self.email);
        a
    }
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn build_user(email: &str, username: &str, active: bool, sign_in_count: i64) -> User {
    User {
        email: String::from(email),
        username: String::from(username),
        active: active,
        sign_in_count: sign_in_count,
    }
}

fn print_user(user: &User) {
    println!("User: {}, {}, {}, {}.",
             user.username,
             user.email,
             user.active,
             user.sign_in_count
    );
}

#[allow(dead_code)]
fn build_user_string(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

#[derive(Debug)]
struct File {
    name: String,
    data: Vec<u8>,
}

struct Hostname(String);

#[allow(dead_code)]
fn connect(host: Hostname) {
    println!("connected to {}", host.0);
}

fn open(f: &mut File) -> bool {
    true
}

fn close(f: &mut File) -> bool {
    true
}

fn read(
    f: &File,
    save_to: &mut Vec<u8>,
) -> usize {
    let mut tmp = f.data.clone();
    let read_length = tmp.len();

    save_to.reserve(read_length);
    save_to.append(&mut tmp);
    read_length
}

impl File {
    // As `File::new()` is a completely normal function--rather than something blessed by the language--we need to tell
    //  Rust that it will be returning a `File` from this function.
    fn new(name: &str) -> File {
        // `File::new()` does little more than encapsulate the object creation syntax.
        File {
            name: String::from(name),
            data: Vec::new(),
        }
    }

    fn new_with_data(name: &str, data: &Vec<u8>) -> File {
        let mut f = File::new(name);
        f.data = data.clone();
        f
    }

    // `File::len()` takes an implicit argument `self`. You'll notice that there is no explicit
    //  argument provided on line 25.
    fn len(&self) -> usize {
       // `usize` is the type returned by `Vec<T>::len()`, which is sent directly through to the caller.
       self.data.len()
    }

    fn read(
        self: &File,
        save_to: &mut Vec<u8>,
    ) -> usize {
        let mut tmp = self.data.clone();
        let read_length = tmp.len();
        save_to.reserve(read_length);
        save_to.append(&mut tmp);
        read_length
    }
}
struct AlwaysEqual;
