fn main() {
    let x = 2;

    match x {
        1 => {
            println!("Test\n");
        },
        2 => println!("Test 2\n"),
        //_ => println!("Anything else\n"),
        _ => (),

    }

    let x = Some(5);
    //let x = None;
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        Some(y) => println!("Matched, y = {:?}", y),
        _ => println!("Default case, x = {:?}", x),
    }

    println!("at the end: x = {:?}, y = {:?}", x, y);

    let x = 1;

    match x {
        1 | 2 => println!("one or two"),
        3 => println!("three"),
        _ => println!("anything"),
    }


    let x = 7;

    match x {
        1..=5 => println!("one through five"),
        1..=9 => println!("one through nine exclusive"),
        _ => println!("something else"),
    }

    let x = 'c';

    match x {
        'a'..='j' => println!("early ASCII letter"),
        'k'..='z' => println!("late ASCII letter"),
        _ => println!("something else"),
    }

    let p = Point {
        x: 0,
        y: 7,
    };

    let Point { x: a, y: b } = p;
    assert_eq!(0, a);
    assert_eq!(7, b);

    // Automatically create new variables with the same names as the struct attributes.
    let Point { x, y } = p;
    assert_eq!(0, x);
    assert_eq!(7, y);

    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }
}

struct Point {
    x: i32,
    y: i32,
}
