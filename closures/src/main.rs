use std::thread;
use std::time::Duration;
use std::collections::HashMap;
use std::hash::Hash;

//fn simulated_expensive_calculation(intensity: u32) -> u32 {
//    println!("Calculating slowly...");
//    thread::sleep(Duration::from_secs(2));
//
//    intensity
//}

struct Cacher<T>
where
    T: Fn(u32) -> u32,
{
    calculation: T,
    value: Option<u32>,
}

impl<T> Cacher<T>
where
    T: Fn(u32) -> u32
{
    fn new(calculation: T) -> Cacher<T> {
        Cacher {
            calculation,
            value: None,
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            }
        }
    }
}

struct CacherHashMap<T, K, V>
where
    T: Fn(K) -> V,
    K: Eq + Hash,
    V: Copy,
{
    calculation: T,
    values_hash_map: HashMap<K, V>,
}

impl<T, K, V> CacherHashMap<T, K, V>
where
    T: Fn(K) -> V,
    K: Hash + Eq + Copy,
    V: Copy,
{
    fn new(calculation: T) -> CacherHashMap<T, K, V> {
        CacherHashMap {
            calculation,
            values_hash_map: HashMap::new(),
        }
    }

    fn value(&mut self, key: K) -> V {
        match self.values_hash_map.get(&key) {
            Some(val) => *val,
            None => {
                let val = (self.calculation)(key);
                self.values_hash_map.insert(key, val);
                val
            }
        }
    }
}

fn generate_workout(intensity: u32, random_number: u32) {
    // Comparison of function and closure syntax.
    // fn  add_one_v1   (x: u32) -> u32 { x + 1 }
    // let add_one_v2 = |x: u32| -> u32 { x + 1 };
    // let add_one_v3 = |x|             { x + 1 };
    // let add_one_v4 = |x|               x + 1  ;


    // Without type annotations.
    let mut expensive_result = CacherHashMap::new(|num| {
        println!("Calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    });
    // With type annotations.
    //let expensive_closure = |num: u32| -> u32 {
    //    println!("calculating slowly...");
    //    thread::sleep(Duration::from_secs(2));
    //    num
    //};

    if intensity < 25 {
        println!("Today, do {} pushups!", expensive_result.value(intensity));
        println!("Next, do {} situps!", expensive_result.value(intensity));
    } else {
        if random_number == 3 {
            println!("Take a break today! Remember to stay hydrated!");
        } else {
            println!(
                "Today, run for {} minutes!",
                expensive_result.value(intensity)
            );
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(unused)]

    use super::*;

    #[test]
    #[should_panic]
    fn call_with_different_values() {
        let mut c = Cacher::new(|a| a);

        let v1 = c.value(1);
        let v2 = c.value(2);

        assert_eq!(v2, 2);
    }

    #[test]
    fn call_with_different_values_hash_map() {
        let mut c = CacherHashMap::new(|a| a);

        let v1 = c.value(1);
        let v2 = c.value(2);

        assert_eq!(v2, 2);
    }

    #[test]
    fn call_with_different_value_types_hash_map() {
        let mut c = CacherHashMap::new(
            |a: &str| a.len(),
    );

        let v1 = c.value("a");
        let v2 = c.value("foobar");

        assert_eq!(v2, 6);
    }

    #[test]
    fn closure_capture_environment() {
        let x = 4;
        let y = 4;

        let equal_to_x = |z| z == x;
        // Function cannot capture their definition environment.
        //fn equal_to_x(z: i32) -> bool {
        //    z == x
        //}

        assert!(equal_to_x(y));
    }

    #[test]
    fn move_closures() {
        let x = vec![1, 2, 3];

        let equal_to_x = move |z| z == x;
        // Cannot use 'x' variable after it has been moved to the move closure.

        let y = vec![1, 2, 3];

        assert!(equal_to_x(y));
    }
}

fn main() {
    let simulated_user_specified_value = 15;
    //let simulated_user_specified_value = 42;
    let simulated_random_number = 7;
    //let simulated_random_number = 3;

    generate_workout(simulated_user_specified_value, simulated_random_number);

}
