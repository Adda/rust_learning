use std::io;

fn main() {
    let mut counter = String::new();
    println!("Enter a number smaller than 20.");
    io::stdin()
        .read_line(&mut counter)
        .expect("Wrong input.");
    let mut counter: u32 = counter.trim().parse().expect("Parse to u32 failed.");
    let loop_result = loop {
        println!("counter: {}.", counter);
        if counter  >= 20 {
            break counter;
        }
        counter += 1;
    };

    println!("The result of the loop: {}.", loop_result);

    // For loop using iter().
    let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

    for number in numbers.iter() {
        println!("Number: {}.", number);
    }

    for number in (1..11).rev() {
        println!("Number in range: {}.", number);
    }

}
