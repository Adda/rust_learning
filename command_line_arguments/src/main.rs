use clap::Parser;

/// Test Application to Parse Command Line Arguments
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = "Parse Command Line Arguments easily and efficiently.")]
struct Config {
    /// File to work with.
    #[clap(short, long)]
    file: String,

    /// Number to work with.
    #[clap(short, long, default_value_t = 0, help="Five less than your favorite number.")]
    number: i32,

    /// Number 2 to work with.
    #[clap(long, default_value_t = 1, help="Other number.")]
    number2: i32,
}

fn main() {
    let args = Config::parse();

    println!("File: {}", args.file);
    println!("Number: {}", args.number);
    println!("Number2: {}", args.number2);
}
