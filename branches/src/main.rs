fn main() {
    let number = 3;

    if number < 5 {
        println!("True condition.");
    } else {
        println!("False condition.");
    }

    let x: i32;
    if number != 6 {
        x = 6;
    } else if number != 4 {
        x = 4;
    } else {
        x = 0;
    }

    println!("The value of x is: {}.", x);

    let condition = true;
    // let condition = false;
    let mut number = if condition { 23 } else { 42 };
    println!("The value of number is: {}.", number);

    let mut counter = 0;
    let res = loop {
        counter += 1;
        println!("New iteration.");

        if counter == 10 {
            break counter * 2
        }
    };

    println!("The result is: {}.", res);

    {
        let mut x = 5;
        loop {
            print!("{x}");
            x += x;
            if x < 100 {
                print!(" ");
                continue;
            }
            println!();
            break;
        }
    }

    while number != 0 {
        println!("The number is: {}.", number);
        number -= 1;
    }

    let a = [10, 20, 30, 40, 50];

    for elem in a.iter() {
        println!("The value of elem is: {}.", elem);
    }

    for_loop_fn(a[0]);
}

fn for_loop_fn(last: i32) {
    for num in 0..last + 1 {
        println!("{}...", num);
    }
    for num in (0..=last).rev() {
        println!("{}...", num);
    }
}

fn _counting() {
    let mut count = 0;
    'counting_up: loop {
        println!("count = {count}");
        let mut remaining = 10;

        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;
            }
            remaining -= 1;
        }

        count += 1;
    }
    println!("End count = {count}");
}
