use std::ops::{Add};
use std::time::{Duration};

fn main() {
    another_function(5, 6.3);

    let y = {
        let mut x = 4;
        x += plus_one(x);
        x * 2
    };

    // let f = x; // 'x' does not exists in this scope.
    println!("The value of y is: {}.", y);
    println!("Return five: {}.", return_five());

    let a = 10;
    let b = 20;
    let res = add_with_lifetimes(&a, &b);
    println!("{}", res);

    let floats = add(1.2, 3.4);
    let ints = add(10, 20);
    let durations = add(
        Duration::new(5, 0),
        Duration::from_secs(10),
    );
    println!("{}", floats);
    println!("{}", ints);
    println!("{:?}", durations);
}

fn another_function(x: i32, y: f64) {
    println!("Hello, world, from another function.");
    println!("The value of x is: {}.", x);
    println!("The value of y is: {}.", y);
}

fn return_five() -> i32 { 5 }

fn plus_one(x: i32) -> i32 { x + 1 }

fn add_with_lifetimes<'a, 'b>(i: &'a i32, j: &'b i32) -> i32 {
    *i + *j
}

/// Add two values.
///
/// # Arguments
///
/// * `i`:
/// * `j`:
///
/// returns: T
///
/// # Examples
///
/// ```
/// add(1, 2)
/// ```
fn add<T: Add<Output = T>>(i: T, j: T) -> T {
    i + j
}