#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.height * self.width
    }

    fn can_hold(&self, second_rect: &Rectangle) -> bool {
        self.width > second_rect.width && self.height > second_rect.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };
    let rect2 = Rectangle {
        width: 10,
        height: 40,
    };
    let rect3 = Rectangle {
        width: 60,
        height: 45,
    };

    println!("The area of the rectangle is {} square pixels.",
             rect1.area());

    println!("The rec1 is: {:?}", rect1);
    println!("The rec1 is: {:#?}", rect1);

    let square = Rectangle::square(15);
    println!("The square is: {:?}", square);

}

