#![allow(unused)]

mod front_of_house;

fn serve_order() {}

pub fn test_function() {
    let time = 0;
    time += 4;
    println!("{}", time);

}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::serve_order();
    }

    fn cook_order() {}

    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }

    pub enum Appetizer {
        Soup,
        Salad,
    }
}

// Use of absolute path:
pub use crate::front_of_house::hosting;
// Use of relative path:
//use self::front_of_house::hosting;

use crate::front_of_house::hosting::add_to_waitlist;

use std::collections::HashMap;

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();

    // Use of 'use' keyword for methods (idiomatic way):
    hosting::add_to_waitlist();

    // Use of path with 'use' keyword:
    add_to_waitlist();

    // Use of 'use' keyword for enums or structs (idiomatic way):
    let mut has_map = HashMap::new();
    has_map.insert(1, 2);

    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    // meal.seasonal_fruit = String::from("blueberries");

    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;
}

// Bring two types with the same name:
use std::fmt::Result;
use std::io::Result as IOResult;

fn function1() -> Result {
    // --snip--
    Ok(())
}

fn function2() -> IOResult<()> {
    // --snip--
    Ok(())
}

//use std::{cmp::Ordering, cmp};
use std::{
    cmp::Ordering,
    cmp,
};

use std::io::{self, Write};

fn main() {
    use std::collections::*;
}
