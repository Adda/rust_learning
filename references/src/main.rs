fn main() {
    let mut s1 = String::from("Hello");

    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);

    change_string(&mut s1);
    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);

    {
        let _r1 = &mut s1;
    } // r1 goes out of scope here, so we can make a new reference with no problems.
    let _r2 = &mut s1;

    let r1 = &s1; // no problem
    let r2 = &s1; // no problem
    println!("{} and {}", r1, r2);
    // r1 and r2 are no longer used after this point

    let r3 = &mut s1; // no problem
    println!("{}", r3);
}

fn calculate_length(s: &String) -> usize { // s is a reference to a String
    s.len()
} // Here, s goes out of scope. But because it does not have ownership of what
  // it refers to, nothing happens.

fn change_string(s: &mut String) {
    s.push_str(", world.");
    s.push_str(&no_dangle());
}

fn no_dangle() -> String {
    let s1 = String::from("123");
    let s2 = String::from("456");
    format!("Text {} new text {}", s1, s2);

    String::from("String moved from within a function.")
}
