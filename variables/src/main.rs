use std::io;

const MAX_POINTS: u32 = 100_000;

fn main() {
    let x: u32;
    x = 42;
    println!("The value of x is: {x}.");

    println!("The value of MAX_POINTS is: {MAX_POINTS}.");

    let mut x = 5;
    println!("The value of x is: {x}.");
    x = 6;
    println!("The value of x is: {x}.");

    let y = 5;
    let y = y + 1;
    let mut y = y * 2;
    println!("The value of y is: {y}.");
    y += MAX_POINTS;
    println!("The value of y with MAX_POINTS is: {y}.");

    let spaces = "     ";
    let spaces = spaces.len();

    let mut spaces: &str = "   ";
    let spaces2: &str = "  ";
    let spaces = format!("{spaces}{spaces2}");
    let spaces = spaces.len();

    // Compile error:
    // let mut spaces = "    ";
    // spaces = spaces.len();

    println!("The length of spaces is: {spaces}.");

    let guess: u32 = "42".parse().expect("Not a number.");
    println!("The guess is: {guess}.");

    let f = 2.0; // f64
    let f_32: f32 = 3.0; // f32
    let remainder = f % f_32;

    println!("The value of remainder is: {remainder}.");
    println!("The type of remainder is: {}.", get_type_name(&remainder));

    let _t = true;
    let _nt: bool = false;

    let _character = 'a';

    let _tuple = (1, 3.4, 'z', "string");
    let tuple: (i32, f64, char, &str) = (1, 3.4, 'z', "string");

    let (_val0, val1, _val2, val3) = tuple;
    println!("The values of tuple are: {}, {}, {}, {}.", tuple.0, val1, tuple.2, val3);

    let _months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
    let array: [i32; 5] = [1, 2, 3, 4, 5];

    let _array2 = [0; 5]; // [0, 0, 0, 0, 0]

    let first_elem = _months[3];
    println!("The month is: {}.", first_elem);

    println!("The valued from given index is: {}.", get_value_from_array(&array));

    let _x = 57u8;
    let _x = 1_000;
    let _x = 0xff;
    let _x = 0o77;
    let _x = 0b0010_1101;
    let x = b'a';
    println!("{x}");

    let _c = 'z';
    let _z: char = 'ℤ'; // with explicit type annotation
    let _heart_eyed_cat = '😻';

    let mut x = String::from("String from string literal.");
    x.push_str(" Append next string literal.");
    println!("{x}");

    {
        // Resource Acquisition Is Initialization (RAII) pattern aplied in Rust ownership system:
        //  Memory is deallocated when it goes out of scope.
        let _v = vec![1, 2, 3];
    }
    let v = vec![1, 2, 3];
    drop(v); // Explicitly drop the vector before its owner variable goes out of scope.

    let x = 5;
    let y = x;
    println!("x: {x} == y: {y} = {}", x == y);

    // Rust never implicitly creates a deep copies. Henceforth, all automatic copying is inexpensive in terms of
    //  performance.
    let s1 = String::from("Original string literal.");
    let s2 = s1;
    println!("s2: '{s2}', but s1 has been moved to s2.");

    let s1 = String::from("Original string literal.");
    let s2 = s1.clone();
    println!("s1: '{s1}', s1: {s2}. s1 has been cloned to s2.");
}

fn get_type_name<T>(_: &T) -> &str {
    return std::any::type_name::<T>();
}

fn get_value_from_array(array: &[i32]) -> i32 {
    println!("Enter an array index");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line.");

    let index: usize = index.trim().parse()
        .expect("Index entered is not a number.");

    array[index]
}

/// Return value from array added to local variable.
#[allow(unused)]
fn get_value_from_array_plus(array: &[i32]) -> i32 {
    let x = {
        let y = 4;
        y + 3
    };

    get_value_from_array(array) + x
}
