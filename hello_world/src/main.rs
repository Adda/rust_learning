pub trait AnyExt {
    fn type_name(&self) -> &'static str;
}

impl<T> AnyExt for T {
    fn type_name(&self) -> &'static str {
        std::any::type_name::<T>()
    }
}

fn test2(ok: bool) -> Result<&'static str, &'static str> {
    match ok {
       true => { Ok("Hello") } |
       false => { Err("Not OK.") }
    }
}

fn main() {
    println!("{}, world!", test2(true).unwrap());
    test();

    let s = "Hello";
    println!("{}", s.type_name());
}

fn test() {
    let y: i64 = 41234324242;
    let mut x: i64 = 42;

    x += 1;
    x += y;

    println!("Test string {}: {} adwa", x, y);
}
