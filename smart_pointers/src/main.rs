mod lib;

use std::rc::Rc;
use std::cell::RefCell;

use lib::List::{Cons, Nil};
use lib::RcList::{Cons as RcCons, Nil as RcNil};
use lib::MyBox;
use lib::CustomSmartPointer;
use lib::RefCellList;


fn main() {
    let b = Box::new(5);
    println!("box = {}.", b);

    let _list = Box::new(Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil)))))));

    let x = 5;
    //let y = &x;
    let y = Box::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y); // Internally, Rust calls `*(y.deref())`.

    let z = MyBox::new(x);

    assert_eq!(5, *z);

    let m = MyBox::new(String::from("Rust"));
    lib::hello(&m);
    lib::hello(&(*m)[..]);


    let a = Rc::new(RcCons(5, Rc::new(RcCons(10, Rc::new(RcNil)))));
    println!("count after creating a = {}", Rc::strong_count(&a));

    let b = RcCons(3, Rc::clone(&a));
    println!("count after creating b = {}", Rc::strong_count(&a));
    {
        let c = RcCons(4, Rc::clone(&a));
        println!("count after creating c = {}", Rc::strong_count(&a));
    }
    println!("count after c goes out of scope = {}", Rc::strong_count(&a));

    let c = CustomSmartPointer {
        data: String::from("my stuff"),
    };
    let _d = CustomSmartPointer {
        data: String::from("other stuff"),
    };

    drop(c);
    println!("CustomSmartPointers created.");

    let value = Rc::new(RefCell::new(5));

    let a = Rc::new(RefCellList::Cons(Rc::clone(&value), Rc::new(RefCellList::Nil)));

    let b = RefCellList::Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
    let c = RefCellList::Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));

    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);
    println!("value after = {:?}", value);

}
