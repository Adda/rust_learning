use std::io;

fn main() {
    println!("Enter a temperature in Celsius.");

    let mut celsius = String::new();
    io::stdin()
        .read_line(&mut celsius)
        .expect("Enter a valid number.");

    let celsius: f64 = celsius.trim().parse()
            .expect("Not a valid number.");

    let fahrenheit = celsius * 9./5. + 32.0;

    println!("The temperature in Fahrenheit is: {fahrenheit}.");


}
