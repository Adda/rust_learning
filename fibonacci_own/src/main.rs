fn fibonacci(number: i32) -> i32 {
    match number {
        0 => 0,
        1 => 1,
        _ => fibonacci(number - 1) + fibonacci(number  - 2)
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let number: i32 = args[1].parse().unwrap();

    for x in 0..=number {
        println!("{result}", result=fibonacci(x));
    }
}
