#![allow(unused)]
fn main() {
    let favorite_color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    if let Some(color) = favorite_color {
        println!("Using your favorite color, {}, as the background", color);
    } else if is_tuesday {
        println!("Tuesday is green day!");
    } else if let Ok(age_val) = age {
        if age_val > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else if let Ok(age) = age {
        // Variable shadowing so we do not need to use a new variable name such as 'age_val'.
        if age == 32 {
            // ...
        }
    } else {
        println!("Using blue as the background color");
    }

    // While let.
    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);

    while let Some(top) = stack.pop() {
        println!("{}", top);
    }

    let v = vec!['a', 'b', 'c'];

    for (index, value) in v.iter().enumerate() {
        println!("{} is at index {}", value, index);
    }

    let x = 5;
    let (x, y, z) = (1, 2, 3);
    println!("{} {} {}", x, y, z);

    let point = (3, 5);
    print_coordinates(&point);

    let point = Point::new(4, 5);
    print_coordinates_point_reference(&point);

    let msg = Message::ChangeColor(0, 160, 255);

    match msg {
        Message::Quit => {
            println!("The Quit variant has no data to destructure.")
        }
        Message::Move { x, y } => {
            println!("Move in the x direction {} and in the y direction {}", x, y);
        }
        Message::Write(text) => println!("Text message: {}", text),
        Message::ChangeColor(r, g, b) => {
            println!("Change the color to red {}, green {}, and blue {}", r, g, b)
        }
    }

    let msg = MessageColour::ChangeColor(Colour::Hsv(0, 160, 255));

    match msg {
        MessageColour::ChangeColor(Colour::Rgb(r, g, b)) => {
            println!("Change the color to red {}, green {}, and blue {}", r, g, b)
        }
        MessageColour::ChangeColor(Colour::Hsv(h, s, v)) => println!(
            "Change the color to hue {}, saturation {}, and value {}",
            h, s, v
        ),
        _ => (),
    }

    let ((feet, inches), Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });

    foo_ignore(3, 4);

    let mut setting_value = Some(5);
    let new_setting_value = Some(10);

    match (setting_value, new_setting_value) {
        (Some(_), Some(_)) => {
            println!("Can't overwrite an existing customized value");
        }
        _ => {
            setting_value = new_setting_value;
        }
    }

    println!("setting is {:?}", setting_value);

    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, _, third, _, fifth) => {
            println!("Some numbers: {}, {}, {}", first, third, fifth)
        }
    }

    let s = Some(String::from("Hello!"));

    //if let Some(_s) = s { // Will produce a bug.
    // Does not bind to a variable, s can be used further in the code.
    if let Some(_) = s {
        println!("found a string");
    }

    println!("{:?}", s);

    let origin = Point4 {
        x: 0,
        y: 2,
        z: 3,
        w: 5,
    };

    match origin {
        Point4 { y, .. } => println!("y is {}", y),
    }

    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, .., last) => {
            println!("Some numbers: {}, {}", first, last);
        }
    }

    let num = Some(4);

    match num {
        Some(x) if x < 5 => println!("less than five: {}", x),
        Some(x) => println!("{}", x),
        None => (),
    }

    let x = Some(5);
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        Some(n) if n == y => println!("Matched, n = {}", n),
        _ => println!("Default case, x = {:?}", x),
    }

    println!("at the end: x = {:?}, y = {}", x, y);

    let x = 4;
    let y = false;
    //let y = true;

    match x {
        4 | 5 | 6 if y => println!("yes"),
        _ => println!("no"),
    }

    enum MessageTest {
        Hello { id: i32 },
    }

    let msg = MessageTest::Hello { id: 5 };

    match msg {
        MessageTest::Hello {
            id: id_variable @ 3..=7,
        } => println!("Found an id in range: {}", id_variable),
        MessageTest::Hello { id: 10..=12 } => {
            println!("Found an id in another range")
        }
        MessageTest::Hello { id } => println!("Found some other id: {}", id),
    }
}

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}

fn foo(x: i32) {
    // code goes here
}

fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Current location: ({}, {})", x, y);
}

fn print_coordinates_point_reference(point: &Point) {
    let Point { x, y } = point;
    println!("Current location: ({}, {})", x, y);
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

enum Colour {
    Rgb(i32, i32, i32),
    Hsv(i32, i32, i32),
}

enum MessageColour {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(Colour),
}

fn foo_ignore(_: i32, y: i32) {
    println!("This code only uses the y parameter: {}", y);
}

struct Point4 {
    x: i32,
    y: i32,
    z: i32,
    w: i32,
}
