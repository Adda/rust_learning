fn main() {
    let test_string = String::from("Hello world how are you");
    let index = first_word_index(&test_string);
    let slice = first_word_slice(&test_string);
    let slice = first_word_slice(&test_string[..]);
    let slice = first_word_slice(&test_string[0..test_string.len()]);

    println!("The index of a first string is {}, and it is word '{}'", index, slice);

    let s = String::from("hello world");

    let hello = &s[0 .. index]; // Alternatively: [.. index]
    let world = &s[index + 1 .. 11]; // Alternatively: [index + 1 ..]
    let _entire_string = &s[..]; // Equal: [0 .. s.len()]

    println!("'{}' and '{}'", hello, world);

    let a = [1, 2, 3, 4, 5];

    let slice = &a[1..3];

    // &a[0..3] == &a[..3];
    // &a[3..a.len()] == &a[3..];
    // &a[0..a.len()] == &a[..];
    println!("Elements of 'a' are: {}, {}.", slice[0], slice[1]);
}

fn first_word_index(s: &str) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}

fn first_word_slice(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }

    &s[..]
}
