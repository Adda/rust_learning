fn main() {
    let val = "Hello, world!".to_string();
    pass_string_into(&val); // `val` passed as a reference which will be copied inside pass_string_into() function.

    pass_string_into(val); // `val` moved into pass_string_into() function.
    // Cannot use `val` anymore.

    pass_string_into("Hello, world, from &str"); // Passed as a &str reference which will be copied into a string inside.
}

fn pass_string_into(val: impl Into<String>) {
    let owned_val: String = val.into();
    do_expensive_stuff_with_string(owned_val);
}

fn do_expensive_stuff_with_string(val: String) {
    println!("{}", val);
}
