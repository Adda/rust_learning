use regex::Regex;
use clap::{App, Arg};
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::io;

fn main() {
    let search_term = "picture";
    let quote = "\
Every face, every shop, bedroom window, public-house, and
dark square is a picture feverishly turned--in search of what?
It is the same with books.
What do we seek through millions of pages?";

    // Version 1.
    let mut line_num = 1;
    for line in quote.lines() {
        if line.contains(search_term) {
            println!("{}: {}", line_num, line);
        }
        line_num += 1;
    }

    // Version 2.
    for (line_num, line) in quote.lines().enumerate() {
        if line.contains(search_term) {
            let line_num = line_num + 1;
            println!("{}: {}", line_num, line);
        }
    }

    // Version 3.
    let context_lines = 2;
    let needle = "oo";
    let haystack = "\
Every face, every shop,
bedroom window, public-house, and
dark square is a picture
feverishly turned--in search of what?
It is the same with books.
What do we seek
through millions of pages?";

    // Store line numbers of lines with needle found within them.
    let mut tags: Vec<usize> = vec![];
    // Store line number and line contents for lines with needle found and their context.
    let mut context: Vec<Vec<(usize, String)>> = vec![];

    for (i, line) in haystack.lines().enumerate() {
        if line.contains(needle) {
            tags.push(i);

            let v = Vec::with_capacity(2 * context_lines + 1);
            context.push(v);
        }
    }

    if tags.is_empty() {
        return;
    }

    for (i, line) in haystack.lines().enumerate() {
        for (j, tag) in tags.iter().enumerate() {
            let lower_bound = tag.saturating_sub(context_lines);
            let upper_bound = tag + context_lines;

            if (i >= lower_bound) && (i <= upper_bound) {
                let local_context = (i, String::from(line));
                context[j].push(local_context);
            }
        }
    }

    for local_context in context.iter() {
        for &(i, ref line) in local_context.iter() {
            let line_num = i + 1;
            println!("{}: {}", line_num, line);
        }
    }

    // Version 4.
    let search_term = "picture";
    let quote = "Every face, every shop, bedroom window, public-house, and
dark square is a picture feverishly turned--in search of what?
It is the same with books. What do we seek through millions of pages?";
    for line in quote.lines() {
        if line.contains(search_term) {
            println!("{}", line);
        }
    }

    // Version 5.
    println!("{}", format!("Version: {version}", version = 5));

    let args = App::new("grep-lite")
        .version("0.1")
        .about("searches for patterns")
        .arg(Arg::with_name("pattern")
                 .help("The pattern to search for")
                 .takes_value(true)
                 .required(true))
        .arg(Arg::with_name("input")
                 .help("File to search")
                 .takes_value(true)
                 .required(false))
        .get_matches();

    let pattern = args.value_of("pattern").unwrap();
    let regex = Regex::new(pattern).unwrap();
    //let regex = Regex::new(r"wha.*").unwrap();

    let input = args.value_of("input").unwrap_or("-");
    if input == "-" {
        let stdin = io::stdin();
        let reader = stdin.lock();
        process_lines(reader, regex);
    } else {
        let f = File::open(input).unwrap();
        let reader = BufReader::new(f);
        process_lines(reader, regex);
    }
}

fn process_lines<T: BufRead + Sized>(reader: T, regex: Regex) {
    for line in reader.lines() {
        let line = line.unwrap();
        let lowercase_line = line.to_lowercase();
        match regex.find(&lowercase_line) {
            Some(_) => println!("{}", line),
            None => (),
        }
    }
}
