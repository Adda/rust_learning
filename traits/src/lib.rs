#![allow(unused)]

use std::fmt::{Display, Debug};

pub trait Summary {
    // Method signature.
    fn summarize_author(&self) -> String;
    
    // Method signature with default implementation.
    fn summarize(&self) -> String {
        format!("(Read more from {}...)", self.summarize_author())
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

// Using default implementation of summarize method from the trait.
//impl Summary for NewsArticle {}

impl Summary for NewsArticle {
    fn summarize_author(&self) -> String {
        format!("{}", self.author)
    }
}

// Using custom implementation of the methods from the Summary trait.
//impl Summary for NewsArticle {
//    fn summarize_author(&self) -> String {
//        self.author
//    }
//
//    fn summarize(&self) -> String {
//        format!("{}, by {} ({}).", self.headline, self.author, self.location)
//    }
//}

pub struct Toot {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub boost: bool,
}

impl Summary for Toot {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }

    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// Trait bound version.
//pub fn notify<T: Summary>(item: &T) {
//    println!("Breaking news! {}", item.summarize());
//}

// Allow item2 to have different type than item1.
pub fn notify2(item1: &impl Summary, item2: &impl Summary) {}

// Force both items to have the same type.
pub fn notify3<T: Summary>(item1: &T, item2: &T) {}

pub fn notify4(item: &(impl Summary + Display)) {}

pub fn notify5<T: Summary + Display>(item: &T) {}

// Simplify this:
//fn some_function<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 {
// Into this:
fn some_function<T, U>(t: &T, u: &U) -> i32
    where T: Display + Clone,
          U: Clone + Debug,
{ 42 }

fn return_default_toot() -> impl Summary {
    Toot {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        boost: false,
    }
}

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}

// Blanket implementation.
//impl<T: Display> ToString for T {
//    // --snip--
//}
