#![allow(unused_variables)]

mod lib;

use lib::Summary;
use lib::Toot;
use lib::NewsArticle;

fn main() {
    let f = File{};
    let mut buffer = vec!();
    let n_bytes = f.read(&mut buffer).unwrap();
    println!("{} byte(s) read from {:?}", n_bytes, f);

    let toot = Toot {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        boost: false,
    };

    println!("1 new toot: {}", toot.summarize());

    let article = NewsArticle {
        headline: String::from("Penguins win the Stanley Cup Championship!"),
        location: String::from("Pittsburgh, PA, USA"),
        author: String::from("Iceburgh"),
        content: String::from(
            "The Pittsburgh Penguins once again are the best \
         hockey team in the NHL.",
        ),
    };

    println!("New article available! {}", article.summarize());

    let s = 3.to_string();
}

#[derive(Debug)]
struct File;

trait Read {
fn read(
    self: &Self,
    save_to: &mut Vec<u8>,
) -> Result<usize, String>;
}

impl Read for File {
    fn read(self: &File, save_to: &mut Vec<u8>) -> Result<usize, String> {
        Ok(0)
    }
}
