use adder;

mod common;

#[test]
fn test_some_function() {
    common::setup();
    assert_eq!(adder::greeting("Adda"), "Hello, Adda!");
}
