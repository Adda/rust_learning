fn main() {
    let s_l: &str = "String literal"; // Allocated on the stack, immutable.
    let mut s: String = String::from(s_l); // Allocated on the heap, mutable.
    s.push_str(", next literal."); // Append to the end of the string.

    println!("{}", s);

    let mut s2 = s; // 's' is invalidated.

    let mut s3 = s2.clone();
    s2.push_str(" s2");
    s3.push_str(" s3");

    println!("{}", s2);
    println!("{}", s3);

    let s = String::from("hello");  // s comes into scope

    takes_ownership(s); // s's value moves into the function...
                                    // ... and so is no longer valid here

    let x = 5;                      // x comes into scope

    makes_copy(x);                  // x would move into the function,
                                    // but i32 is Copy, so it's okay to still
                                    // use x afterward
    let mut s_valid = String::from("hello");  // s_valid comes into scope
    do_not_takes_ownership(&s_valid);
    s_valid.insert_str(0, "still valid ");
    println!("{}", s_valid);

    let _s1 = gives_ownership();         // gives_ownership moves its return
                                        // value into s1

    let s2 = String::from("hello");     // s2 comes into scope

    let _s3 = takes_and_gives_back(s2);  // s2 is moved into
                                        // takes_and_gives_back, which also
                                        // moves its return value into s3
    let s1 = String::from("hello");

    let (s2, len) = calculate_length(s1);

    println!("The length of '{}' is {}.", s2, len);
} // Here, s3 goes out of scope and is dropped. s2 goes out of scope but was
  // moved, so nothing happens. s1 goes out of scope and is dropped.
  // x goes out of scope, then s. But because s's value was moved, nothing
  // special happens.

fn takes_ownership(some_string: String) { // some_string comes into scope
    println!("{}", some_string);
} // Here, some_string goes out of scope and `drop` is called. The backing
  // memory is freed.

fn do_not_takes_ownership(some_string: &String) { // some_string comes into scope
    println!("{}", some_string);
} // Although the function ends and some_string goes out of scope, only the reference is dropped,
  // but the original string being referenced is still valid in its original scope.

fn makes_copy(some_integer: i32) { // some_integer comes into scope
    println!("{}", some_integer);
} // Here, some_integer goes out of scope. Nothing special happens.


fn gives_ownership() -> String {             // gives_ownership will move its
                                             // return value into the function
                                             // that calls it

    let some_string = String::from("hello"); // some_string comes into scope

    return some_string                       // some_string is returned and
                                             // moves out to the calling
                                             // function
}

// takes_and_gives_back will take a String and return one
fn takes_and_gives_back(a_string: String) -> String { // a_string comes into
                                                      // scope

    a_string  // a_string is returned and moves out to the calling function
}

fn calculate_length(s: String) -> (String, usize) {
    let length = s.len(); // len() returns the length of a String

    (s, length)
}
