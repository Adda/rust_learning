pub struct Post {
    minimal_approves_amount_to_publish: u32,
    content: String,
    state: Option<Box<dyn State>>,
    approves_amount: u32,
}

impl Post {
    pub fn new() -> Post {
        Post {
            content: String::new(),
            state: Some(Box::new(Draft {})),
            approves_amount: 0,
            minimal_approves_amount_to_publish: 2,
        }
    }

    pub fn content(&mut self) -> &str {
        self.state.as_mut().unwrap().content(&self)
    }

    pub fn request_review(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review())
        }
    }

    pub fn approve(&mut self) {
        self.approves_amount += 1;

        if self.approves_amount >= self.minimal_approves_amount_to_publish {
            if let Some(s) = self.state.take() {
                self.state = Some(s.approve())
            }
        }
    }

    pub fn reject(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.reject())
        }
    }

    pub fn add_text(&mut self, text: &str) {
        self.push_content(text)
    }

    fn push_content(&mut self, text: &str) {
        self.content.push_str(text);
    }

}

trait State {
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;

    fn reject(self: Box<Self>) -> Box<dyn State> {
        Box::new(Draft {})
    }

    fn content<'a>(&self, _post: &'a mut Post) -> &'a str {
        ""
    }
}

struct Draft {}

impl Draft {
    fn add_text(post: &mut Post, text: &str) {
        post.push_content(text);
    }
}

impl State for Draft {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview {})
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

}

struct PendingReview {}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        Box::new(Published {})
    }
}

struct Published {}

impl State for Published {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn content<'a>(&self, post: &'a mut Post) -> &'a str {
        post.content = String::from("aawd");
        &post.content
    }
}

#[cfg(test)]
mod tests {
    use crate::Post;

    #[test]
    fn empty_draft_returns_empty_string() {
        let draft = Post::new();
        assert_eq!(draft.content(), "");
    }

    #[test]
    fn draft_post_returns_empty_string() {
        let mut draft = Post::new();
        draft.add_text("Hello");
        assert_eq!(draft.content(), "");
    }

    #[test]
    fn reject_approved_post() {
        let mut post = Post::new();
        post.add_text("Hello");
        post.request_review();
        post.approve();
        post.reject();
        assert_eq!("", post.content());
    }

    #[test]
    fn reject_pending_post() {
        let mut post = Post::new();
        post.add_text("Hello");
        post.request_review();
        post.reject();
        assert_eq!("", post.content());
    }

    #[test]
    fn update_rejected_post() {
        let mut post = Post::new();
        post.add_text("Hello");
        post.request_review();
        post.reject();
        post.add_text("Hello");
        post.approve();
        assert_eq!("HelloHello", post.content());
    }

    #[test]
    fn approve_multiple_times_before_published() {
        let mut post = Post::new();
        post.add_text("Hello");
        post.request_review();

        post.approve();
        assert_eq!("", post.content());

        post.approve();
        assert_eq!("Hello", post.content());
    }
}
