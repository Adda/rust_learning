use std::thread;
use std::sync::{Mutex, Arc, mpsc};
use std::time::Duration;

fn main() {
    let v  = vec![1, 2, 3];
    let thread_handle = thread::spawn(move || {
        for i in 0..10 {
            println!("Hi from spawned thread with i = {}.", i);
            if i == 0 {
                println!("The vector v = {:?}.", v);
            }
            thread::sleep(Duration::from_millis(1));
        }
    });

    //thread_handle.join().unwrap();

    for i in 0..5 {
        println!("Hi from main thread with i = {}", i);
        thread::sleep(Duration::from_millis(1));
    }

    thread_handle.join().unwrap();

    // tx: Transmitter.
    // rx: Receiver.
    let (tx, rx) = mpsc::channel();
    let tx2 = tx.clone();
    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            tx2.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    let thread_handle = thread::spawn( move || {
        let val = String::from("Temporary string sent from the spawned thread");
        tx.send(val).unwrap();

        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    //let received = rx.recv().unwrap();
    for received in rx {
        println!("Got message: {}.", received);
    }

    thread_handle.join().unwrap();

    let mutex = Mutex::new(5);

    {
        let mut num = mutex.lock().unwrap();
        *num = 6;
    }

    println!("Number: {:?}.", mutex);

    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
