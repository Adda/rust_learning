// Given a list of integers, use a vector and return:
// - the mean (the average value),
// - median (when sorted, the value in the middle position), and
// - mode (the value that occurs most often;
//         a hash map will be helpful here)
// of the list.

fn main() {
    let mut vec_of_integers: Vec<i32> = vec![1, 4, 6, 5, 3, 8, 1, 2, 6, 4, 5, 6, 8, 8, 8];

    // Mean.
    let mut sum = 0;
    for val in &vec_of_integers {
        sum += val;
    }
    println!("Sum: {}.", sum);

    let length = vec_of_integers.len();
    let mean = sum / length as i32;
    println!("Mean: {}.", mean);

    // Median.
    vec_of_integers.sort();

    let median = vec_of_integers[length / 2];
    println!("Median: {}", median);

    // Mode.
    use std::collections::HashMap;
    let mut value_occurance: HashMap<i32, u32> = HashMap::new();

    for val in &vec_of_integers {
        let value_entry = value_occurance.entry(*val).or_insert(0);
        *value_entry += 1;
    }

    let mut maximal_elem = None;
    for elem in value_occurance.iter() {
        match maximal_elem {
            None => maximal_elem = Some(elem),
            Some(max_elem) => {
                if elem.1 > max_elem.1 {
                    maximal_elem = Some(elem);
                }

            }
        }
    }

    match maximal_elem {
        None => println!("No mode."),
        Some(maximal_elem) => println!("Mode: {}.", maximal_elem.0),

    }
}
