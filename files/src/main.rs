#![allow(unused_variables)]

use std::fmt::Debug;

type File = String;

fn open(f: &mut File) -> bool {
    true   // <3>
}

fn close(f: &mut File) -> bool {
    true
}

#[allow(dead_code)]
fn read(f: &mut File,
        save_to: &mut Vec<u8>) -> ! {
    unimplemented!()
}

fn main() {
    let mut f1 = File::from("f1.txt");
    open(&mut f1);
    //read(f1, vec![]);
    close(&mut f1);

    dead_end()
}

fn report<T: Debug>(item: T) {
    println!("{:?}", item);
}

fn clear(text: &mut String) -> () {
    *text = String::from("");
}

fn dead_end() -> ! {
    panic!("you have reached a dead end");
}

fn forever() -> ! {
    loop {
        //...
    };
}
