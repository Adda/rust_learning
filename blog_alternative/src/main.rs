use blog_alternative::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch today.");

    let post = post.request_review();

    let mut post = post.reject();

    post.add_text(" Updated post text.");

    let mut post = post.request_review();

    post.approve();
    post.approve();

    let post = post.publish();

    assert_eq!("I ate a salad for lunch today. Updated post text.", post.content());
}
