pub struct Post {
    content: String,
}

pub struct DraftPost {
    content: String,
}

impl Post {
    pub fn new() -> DraftPost {
        DraftPost {
            content: String::new(),
        }
    }

    pub fn content(&self) -> &str {
        &self.content
    }
}

impl DraftPost {
    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn request_review(self) -> PendingReviewPost {
        PendingReviewPost {
            content: self.content,
            minimal_approves_amount: 2,
            approves: 0,
        }
    }
}

pub struct PendingReviewPost {
    content: String,
    minimal_approves_amount: u32,
    approves: u32,
}

impl PendingReviewPost {
    pub fn approve(&mut self) {
        self.approves += 1;
    }

    fn is_publishable(&self) -> bool {
        self.approves > self.minimal_approves_amount
    }

    pub fn publish(self) -> Post {
        Post {
            content: self.content,
        }
    }

    pub fn reject(self) -> DraftPost {
        DraftPost {
            content: self.content,
        }
    }
}
