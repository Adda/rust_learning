fn main() {
    let vec1: Vec<i32> = Vec::new();
    let mut vec2 = vec![1, 2, 3];

    vec2.push(1);
    vec2.push(2);
    vec2.push(3);

    let mut vec3 = Vec::new();
    vec3.push(1);

    let v = vec![1, 2, 3, 4, 5];

    let third: &i32 = &v[2]; // Reference to the element.
    println!("The third element is {}", third);

    {
    let third: i32 = v[2];
    println!("The third element is {}", third);
    }

    let third2 = v[2];
    println!("The third element is {}", third2);

    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element."),
    }

    let v = vec![1, 2, 3, 4, 5];

    // Panicks.
    //let does_not_exist = &v[100];

    // Returns None.
    let does_not_exist = v.get(100);
    match does_not_exist {
        Some(&val) => println!("The value is: {}", val),
        None => println!("The value does not exists."),
    }

    let mut v = vec![1, 2, 3, 4, 5];

    //let first = &v[0];
    //v.push(6);
    //println!("The first element is: {}", first);

    let first = v[0];
    v.push(6);
    println!("The first element is: {}", first);

    for item in &v {
        println!("Item is: {}", item);
    }

    for item in &mut v {
        *item += 50;
    }
    // Vector 'v' has been moved, 'v' is now invalid.
    for item in v {
        println!("Item is: {}", item);
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
}

enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

