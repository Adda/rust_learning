use std::thread;
use std::time::{Duration, Instant};

fn main() {
    let mut collection = [3_u32; 2];

    for item in &collection { // This equals
        println!("{}", item);
    }
    for item in collection.iter() { // this.
        println!("{}", item);
    }

    for item in &mut collection { // This equals
        *item += 4;
        println!("{}", item);
    }
    for item in collection.iter_mut() { // this.
        *item += 4;
        println!("{}", item);
    }

    for item in collection { // This equals
        println!("{}", item);
    }
    for item in collection.into_iter() { // this.
        println!("{}", item);
    }

    println!("{:?}", collection);

    for _ in 2..6 {
        println!("Something");
    }

    for _ in 2..=6 {
        println!("One more of Something");
    }

    let collection = [1, 2, 3, 4, 5];
    for i in 0..collection.len() { // Discouraged.
        let item = collection[i];
        println!("{}", item);
    }

    for i in 0..=10 {
        if i > 5 {
            continue;
        }
    }

    let mut x = 4;
    while x > 0 {
        print!("{:?} ", x);
        x -= 1;
    }
    println!();

    let time_limit = Duration::new(1, 0);
    let start = Instant::now();
    while Instant::now() - start < time_limit {
        println!("{}", x);
        x += 1;
        thread::sleep(Duration::new(0, 100_000_000));
    }

    let mut x = 4;
    loop {
        println!("{}", x);
        x -= 1;
        if x == 0 {
            break;
        }
    }

    for (x, y) in (0..).zip(0..) {
        if x + y > 100 {
            break;
        }
        println!("{} + {} = {}", x, y, x + y);
    }

    'outer: for x in 0.. {
        for y in 0.. {
            for z in 0.. {
                if x + y + z > 10 {
                    println!("breaking at {} {} {}", x, y, z);
                    break 'outer;
                }
                println!("{} {} {}", x, y, z);
            }
            continue 'outer;
        }
    }

    let item = 32;
    if item == 42 {} else if item == 132 {} else {}

    let n = 123456;
    let description = if is_even(n) {
        "even"
    } else {
        "odd"
    };
    println!("{} is {}", n, description);

    let n = 654321;
    let description = match is_even(n) {
        true => "even",
        false => "odd",
    };
    println!("{} is {}", n, description);

    let n = loop {
        break 123;
    };
    println!("{}", n);

    let test = match item {
        0 => {}
        10..=20 => {}
        40 | 80 => {}
        _ => {}
    };
    println!("{:?}", test); // Unit type ().

    let needle = 42;
    let haystack = [1, 1, 2, 5, 14, 42, 132, 429, 1430, 4862];
    for item in &haystack {
        let result = match item {
            42 | 132 => "hit!",
            _ => "miss",
        };
        match result {
            "hit!" => println!("{}: {}", item, result),
            "miss" => println!("{}: {}", item, result),
            _ => println!("Anything else: {} {}", item, result),
        };
    }
}

fn is_even(n: i32) -> bool {
    n % 2 == 0
}