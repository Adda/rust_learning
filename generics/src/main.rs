fn largest_i32(list: &[i32]) -> i32 {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn largest_char(list: &[char]) -> char {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T { &self.x }

    fn y(&self) -> &T { &self.y }
}

impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

struct VariablePoint<T, U> {
    x: T,
    y: U,
}

impl<T, U> VariablePoint<T, U> {
    fn x(&self) -> &T { &self.x }

    fn y(&self) -> &U { &self.y }

    fn mixup<V, W>(self, other: VariablePoint<V, W>) -> VariablePoint<T, W> {
        VariablePoint {
            x: self.x,
            y: other.y,
        }
    }
}

fn main() {
    // Functions using generics.
    let number_list = vec![34, 50, 25, 100, 65];
    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest_i32(&number_list); println!("The largest number is {}", result); assert_eq!(result, 100);

    let result = largest_char(&char_list);
    println!("The largest char is {}", result);
    assert_eq!(result, 'y');

    let result = largest(&number_list);
    println!("The largest number is {}", result);
    assert_eq!(result, 100);

    let result = largest(&char_list);
    println!("The largest char is {}", result);
    assert_eq!(result, 'y');

    // Structs using generics.
    let integer = Point { x: 5, y: 10 };
    println!("Point integer.x = {}", integer.x);
    println!("Point integer.y = {}", integer.y);
    let float = Point { x: 1.0, y: 4.0 };
    println!("Distance from origin: {}", float.distance_from_origin());
    //let wont_work = Point { x: 5, y: 10.0 };
    let will_work = VariablePoint { x: 5, y: 10.0 };

    // Generic methods using generics in parameters.
    let p1 = VariablePoint { x: 5, y: 10.4 };
    let p2 = VariablePoint { x: "Hello", y: 'c' };

    let p3 = p1.mixup(p2);
    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);

    let p1 = VariablePoint { x: 5, y: 10.4 };
    let p2 = VariablePoint { x: "Hello", y: 'c' };

    let p4 = p2.mixup(p1);
    println!("p4.x = {}, p4.y = {}", p4.x, p4.y);
}
