// Print n-th Fibonacci number.

use std::io;

fn main() {
    let mut required_index = String::new();
    io::stdin()
        .read_line(&mut required_index)
        .expect("Reading user input failed.");
    let required_index: u32 = required_index.trim().parse().expect("Conversion to number failed.");

    let mut fibonacci_number = 0;
    let mut first = 1;
    let mut second = 1;
    for _ in 1..required_index-1 {
        fibonacci_number = first + second;
        first = second;
        second = fibonacci_number;
    }

    println!("Fibonacci number: {fibonacci_number}.");
}
