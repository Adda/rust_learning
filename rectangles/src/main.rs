// Version using separate parameters.

// fn main() {
// let width1 = 30;
// let height1 = 50;
//
// println!(
// "The area of the rectangle is {} square pixels.",
// area(width1, height1)
// );
// }
//
// fn area(width: u32, height: u32) -> u32 {
// width * height
// }

// Version using tuples.

// fn main() {
// let rect1 = (30, 50);
//
// println!(
// "The area of the rectangle is {} square pixels.",
// area(rect1)
// );
// }
//
// fn area(dimensions: (u32, u32)) -> u32 {
// dimensions.0 * dimensions.1
// }

// Version using structs.

/// Rectangle structure used to store rectangles of certain height and width.
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn width(&self) -> bool {
        self.width > 0
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Self {
        Self {
            width: size,
            height: size,
        }
    }
}

impl Rectangle {
    fn new(width: u32, height: u32) -> Self {
        Self { width, height }
    }
}

fn main() {
    let sq = Rectangle::square(3);
    println!("{:?}", sq);

    let _new_rectangle = Rectangle::new(3, 2);

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    if rect1.width() {
        println!("The rectangle has a nonzero width; it is {}", rect1.width);
    }

    println!(
        "The area of the rectangle is {} square pixels.",
        area(&rect1)
    );

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );

    println!("rect1 is {:?}", rect1);
    println!("rect1 is {:#?}", rect1);

    let scale = 2;
    let rect1 = Rectangle {
        width: dbg!(30 * scale),
        height: 50,
    };

    dbg!(&rect1);

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };
    let rect2 = Rectangle {
        width: 10,
        height: 40,
    };
    let rect3 = Rectangle {
        width: 60,
        height: 45,
    };

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));

    let _q: u32;
    _q = 42;
}

fn area(rectangle: &Rectangle) -> u32 {
    let x = 3;
    println!("x = {x}");
    rectangle.width * rectangle.height
}
